<?php
/**
 * fond module config
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 19 June 2019, 17:39 WIB
 * @link https://bitbucket.org/ommu/fond
 *
 */

return [
	'id' => 'fond',
	'class' => ommu\fond\Module::className(),
];